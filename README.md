Sample applications and jupyter notebooks using the Spatial and Temporal Analysis and Mapping Python Suite.

This is a companion repository to [stamps](https://gitlab.com/STEMLabTW/stamps)
